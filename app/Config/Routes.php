<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Portal::index');

$routes->get('home', 'Portal::index');
$routes->get('daftar', 'Portal::daftar');
$routes->post('daftar/umkm', 'Portal::store');
$routes->post('datakelurahan', 'Portal::getKelurahan');
$routes->post('datakodepos', 'Portal::getKodepos');

$routes->get('admin/home', 'Home::index');
$routes->get('/admin', 'Home::index');

$routes->get('login', 'Auth::index');
$routes->get('logout', 'Auth::logout');
$routes->post('/auth/login', 'Auth::login');

$routes->get('register', 'User::index');
$routes->post('/register/save', 'User::save');

//Route Sektor
$routes->get('/admin/sektor', 'Sektor::index');
$routes->get('/admin/sektor/create', 'Sektor::create');
$routes->get('/admin/sektor/edit/(:any)', 'Sektor::edit/$1');
$routes->post('/admin/sektor/store', 'Sektor::store');
$routes->post('/admin/sektor/update/(:any)', 'Sektor::update/$1');
$routes->get('/admin/sektor/delete/(:any)', 'Sektor::delete/$1');

//Route UMKM
$routes->get('/admin/umkm', 'Umkm::index');
$routes->get('/admin/umkm/create', 'Umkm::create');
$routes->get('/admin/umkm/edit/(:any)', 'Umkm::edit/$1');

//Route Wilayah
$routes->get('/admin/wilayah', 'Wilayah::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
