<?php
namespace App\Controllers;

use App\Models\UmkmModel;

class Home extends BaseController
{

    protected $umkm;

    public function __construct()
    {
        $this->umkm = new UmkmModel();
    }

    public function index()
    {
        session()->setFlashdata('menu', 'Dashboard');
        session()->setFlashdata('breadcrumb-item', 'Home');
        session()->setFlashdata('breadcrumb-active', 'Dashboard');
        $data['pelaku_usaha'] = $this->umkm->countPelakuUsaha();
        $data['usaha'] = $this->umkm->countUsaha();
        $data['jenis_usaha'] = $this->umkm->countJenisUsaha();
        $data['sektor'] = $this->umkm->countSektor();
        $data['kl_mikro'] = $this->umkm->countKlasifikasi("Mikro");
        $data['kl_kecil'] = $this->umkm->countKlasifikasi("Kecil");
        $data['kl_menengah'] = $this->umkm->countKlasifikasi("Menengah");

        return view('admin/home', $data);
    }
}
