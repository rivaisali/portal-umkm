<?php

namespace App\Controllers;

use App\Models\WilayahModel;

class Wilayah extends BaseController
{
    protected $wilayah;

    public function __construct()
    {
        $this->wilayah = new WilayahModel();
    }

    public function index()
    {
        session()->setFlashdata('menu', 'Data Wilayah');
        session()->setFlashdata('breadcrumb-item', 'Wilayah');
        session()->setFlashdata('breadcrumb-active', 'Data Wilayah');
        $data['wilayah'] = $this->wilayah->findAll(50);
        return view('admin/wilayah/index', $data);
    }

}
