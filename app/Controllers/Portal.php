<?php

namespace App\Controllers;

use App\Models\PelakuusahaModel;
use App\Models\UsahaModel;

class Portal extends BaseController
{

    protected $pelakuusaha;
    protected $usaha;

    public function __construct()
    {

        $this->pelakuusaha = new PelakuusahaModel();
        $this->usaha = new UsahaModel();
    }

    public function index()
    {

        return view('home');
    }
    public function daftar()
    {
        $t = $this->request->getVar('t');
        if ($t == null || $t == "") {
            echo "<script>
            var no_wa = '6281143101000';
            location.replace('https://wa.me/'+no_wa+'?text=1');</script>";

        } else {
            $db = \Config\Database::connect();
            $builder = $db->table('wilayah');
            $builder->where(['kodekabkota' => 'Kota']);
            $builder->where(['namakabkota' => 'Gorontalo']);
            $builder->groupBy('namakecamatan');
            $query = $builder->get();
            $data['wilayah'] = $query->getResult();

            $builder = $db->table('sektor');
            $query = $builder->get();
            $data['sektor'] = $query->getResult();

            return view('daftar', $data);
        }
    }

    public function getKelurahan()
    {
        $db = \Config\Database::connect();
        $kdkec = $this->request->getPost('id');
        $builder = $db->table('wilayah');
        $builder->where(['namakabkota' => 'Gorontalo']);
        $builder->where(['namakecamatan' => $kdkec]);
        $query = $builder->get();
        $data = $query->getResult();
        echo json_encode($data);
    }

    public function getKodepos()
    {
        $db = \Config\Database::connect();
        $kd_wilayah = $this->request->getPost('id');
        $builder = $db->table('wilayah');
        $builder->where(['namakabkota' => 'Gorontalo']);
        $builder->where('kd_wilayah', $kd_wilayah);
        $query = $builder->get();
        $data = $query->getResult();
        echo json_encode($data);
    }

    public function store()
    {

        if (!$this->validate([
            'nik' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ],
            ],
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ],
            ],
            'mail' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'valid_email' => 'Email tidak valid',
                ],
            ],
            'notelp' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ],
            ],

            'token' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Token tidak boleh kosong!',
                ],
            ],

            //validasi Upload
            'filektp' => [
                'rules' => 'uploaded[filektp]|max_size[filektp,1024]|is_image[filektp]|mime_in[filektp,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'Foto KTP Harus diuplaod',
                    'max_size' => 'Ukuran Foto KTP Max 1 MB',
                    'is_image' => 'File yang diupload harus gambar',
                    'mime_in' => 'File yang diupload bukan gambar',
                ],
            ],
            'filetempatusaha' => [
                'rules' => 'uploaded[filetempatusaha]|max_size[filetempatusaha,1024]|is_image[filetempatusaha]|mime_in[filetempatusaha,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'Foto Tempat Usaha Harus diuplaod',
                    'max_size' => 'Ukuran Foto Tempat Usaha Max 1 MB',
                    'is_image' => 'File yang diupload harus gambar',
                    'mime_in' => 'File yang diupload bukan gambar',
                ],
            ],
            'filesuket' => [
                'rules' => 'max_size[filesuket,1024]|is_image[filesuket]|mime_in[filesuket,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'Ukuran Surat Keterangan Max 1 MB',
                    'is_image' => 'File yang diupload harus gambar',
                    'mime_in' => 'File yang diupload bukan gambar',
                ],
            ],

        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $token = $this->request->getVar('token');
        $telp = $this->request->getVar('notelp');
        $params = array(
            "token" => $token,
            "NIK" => $this->request->getVar('nik'),

        );
        $post = $this->curlPostRequest($params);

        if ($post->code == 400) {
            session()->setFlashdata('error', "Gagal Melakukan pendaftaran Token Error, Silahkan coba lagi");
            return redirect()->to('/daftar?u=' . $telp . '&t=' . $token);

        } else {
            $fotoktp = $this->request->getFile('filektp');
            $name_fotoktp = $fotoktp->getRandomName();
            $fotoktp->move("upload/ktp", $name_fotoktp);

            $tempatusaha = $this->request->getFile('filetempatusaha');
            $name_tempatusaha = $tempatusaha->getRandomName();
            $tempatusaha->move("upload/tempat_usaha", $name_tempatusaha);

            $suket = $this->request->getFile('filesuket');
            if ($suket->getError() == 4) {
                $name_suket = "-";
            } else {
                $name_suket = $suket->getRandomName();
                $suket->move("upload/suket", $name_suket);
            }

            $this->pelakuusaha->insert([
                'nik' => $this->request->getVar('nik'),
                'nama' => $this->request->getVar('nama'),
                'npwp' => preg_replace("/[^0-9]/", "", $this->request->getVar('npwp')),
                'email' => $this->request->getVar('mail', FILTER_SANITIZE_EMAIL),
                'notelp' => $this->request->getVar('notelp'),
            ]);

            $this->usaha->insert([
                'nik' => $this->request->getVar('nik'),
                'nama_usaha' => $this->request->getVar('nama_usaha'),
                'alamat' => $this->request->getVar('alamat'),
                'kd_daerah' => $this->request->getVar('kdkelurahan'),
                'kodepos' => $this->request->getVar('kodepos'),
                'jenis_usaha' => $this->request->getVar('jenis_usaha'),
                'kd_sektor' => $this->request->getVar('sektor'),
                'modal_usaha' => preg_replace("/[^0-9]/", "", $this->request->getVar('modalusaha')),
                'klasifikasi_usaha' => $this->request->getVar('klasifikasiusaha'),
                'thn_awal_usaha' => $this->request->getVar('tahun_awal'),
                'tk_pria' => $this->request->getVar('tk_pria'),
                'tk_wanita' => $this->request->getVar('tk_wanita'),
                'nm_izin' => $this->request->getVar('jenis_izin'),
                'no_izin' => $this->request->getVar('no_izin'),
                'nib' => $this->request->getVar('nib'),
                'tgl_terbit_nib' => $this->request->getVar('tgl_terbit_nib'),
                'foto_ktp' => $name_fotoktp,
                'tempat_usaha' => $name_tempatusaha,
                'surat_ket' => $name_suket,
            ]);
            echo "<script>
            var no_wa = '6281143101000';
            location.replace('https://wa.me/'+no_wa);</script>";
            session()->setFlashdata('message', 'Terima kasih, anda sudah mendaftarkan usaha anda.<br>');
            return redirect()->to('/daftar?u=' . $telp . '&t=' . $token);

        }
    }

    public function curlPostRequest($params)
    {
        /* Endpoint */
        $url = 'https://reg.balesin.id/submit/umkm-gorontalo';
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result);

    }

}
