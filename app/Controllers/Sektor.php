<?php

namespace App\Controllers;

use App\Models\SektorModel;

class Sektor extends BaseController
{
    protected $sektor;

    public function __construct()
    {
        $this->sektor = new SektorModel();

    }

    public function index()
    {
        session()->setFlashdata('menu', 'Data Sektor');
        session()->setFlashdata('breadcrumb-item', 'Sektor');
        session()->setFlashdata('breadcrumb-active', 'Data Sektor');
        $data['sektor'] = $this->sektor->findAll();
        return view('admin/sektor/index', $data);
    }

    public function create()
    {
        session()->setFlashdata('menu', 'Entri Sektor');
        session()->setFlashdata('breadcrumb-item', 'Sektor');
        session()->setFlashdata('breadcrumb-active', 'Entri Data Sektor');

        return view('admin/sektor/create');
    }

    public function store()
    {
        if (!$this->validate([
            'nm_sektor' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ],
            ],

        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
            
        }


        $this->sektor->insert([
            'nm_sektor' => $this->request->getVar('nm_sektor'),
        ]);
        session()->setFlashdata('message', 'Tambah Data Sektor Berhasil');
        return redirect()->to('/admin/sektor');
    }

    public function edit($id)
    {
        $dataSektor = $this->sektor->find($id);
        if (empty($dataSektor)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Sektor Tidak ditemukan !');
        }
        $data['sektor'] = $dataSektor;
        return view('admin/sektor/edit', $data);
    }

    public function update($id)
    {
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} Harus diisi',
                ],
            ],

        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back();
        }

        $this->sektor->update($id, [
            'nm_sektor' => $this->request->getVar('nama'),
        ]);
        session()->setFlashdata('message', 'Update Data Sektor Berhasil');
        return redirect()->to('/admin/sektor');
    }

    public function delete($id)
    {
        $dataSektor = $this->sektor->find($id);
        if (empty($dataSektor)) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Sektor Tidak ditemukan !');
        }
        $this->sektor->delete($id);
        session()->setFlashdata('message', 'Delete Data Sektor Berhasil');
        return redirect()->to('/admin/sektor');
    }

}
