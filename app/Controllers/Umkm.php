<?php

namespace App\Controllers;

use App\Models\UmkmModel;

class Umkm extends BaseController
{
    protected $umkm;

    public function __construct()
    {
        $this->umkm = new UmkmModel();
    }

    public function index()
    {
        session()->setFlashdata('menu', 'Data UMKM');
        session()->setFlashdata('breadcrumb-item', 'UMKM');
        session()->setFlashdata('breadcrumb-active', 'Data UMKM');
        $data['umkm'] = $this->umkm->getUsaha();
        return view('admin/umkm/index', $data);
    }

}
