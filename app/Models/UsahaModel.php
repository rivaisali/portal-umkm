<?php

namespace App\Models;

use CodeIgniter\Model;

class UsahaModel extends Model
{
    protected $table = "usaha";
    protected $primaryKey = "kd_usaha";
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['kd_usaha', 'nik', 'nama_usaha', 'jenis_usaha', 'klasifikasi_usaha', 'kd_sektor', 'kd_daerah', 'kodepos', 'alamat', 'tk_pria', 'tk_wanita', 'nm_izin', 'no_izin', 'modal_usaha', 'thn_awal_usaha', 'nib', 'tgl_terbit_nib', 'foto_ktp', 'tempat_usaha', 'surat_ket'];
}
