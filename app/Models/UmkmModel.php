<?php

namespace App\Models;

use CodeIgniter\Model;

class UmkmModel extends Model
{
    protected $table = "usaha";

    public function getUsaha()
    {
        $builder = $this->builder();
        $builder->select('*');
        $builder->join('pelaku_usaha', 'pelaku_usaha.nik = usaha.nik');
        $builder->join('sektor', 'sektor.kd_sektor = usaha.kd_sektor');
        $query = $builder->get();
        $data = $query->getResult();
        return $data;
    }

    public function countPelakuUsaha()
    {
        $builder = $this->builder();
        $builder->select('pelaku_usaha.nik');
        $builder->from('pelaku_usaha');
        $builder->distinct();
        $count = $builder->countAllResults();
        if ($count) {
            return $count;
        }
    }

    public function countKlasifikasi($klasifikasi)
    {
        $builder = $this->builder();
        $builder->where('klasifikasi_usaha', $klasifikasi);
        $count = $builder->countAllResults();
        if ($count) {
            return $count;
        }
    }

    public function countUsaha()
    {
        $builder = $this->builder();
        $builder->select('a.nama_usaha');
        $builder->from('usaha as a');
        $builder->distinct();
        $count = $builder->countAllResults();
        if ($count) {
            return $count;
        }
    }

    public function countJenisUsaha()
    {
        $builder = $this->builder();
        $builder->select('a.jenis_usaha');
        $builder->from('usaha as a');
        $builder->distinct();
        $count = $builder->countAllResults();
        if ($count) {
            return $count;
        }
    }

    public function countSektor()
    {
        $builder = $this->builder();
        $builder->select('a.kd_sektor');
        $builder->from('usaha as a');
        $builder->distinct();
        $count = $builder->countAllResults();
        if ($count) {
            return $count;
        }
    }
}
