<?php

namespace App\Models;

use CodeIgniter\Model;

class PelakuusahaModel extends Model
{
    protected $table = "pelaku_usaha";
    protected $primaryKey = "	kd_pelaku_usaha";
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['nik', 'nama', 'npwp', 'email', 'notelp'];
}
