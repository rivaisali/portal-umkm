<?php

namespace App\Models;

use CodeIgniter\Model;

class WilayahModel extends Model
{
    protected $table = "wilayah";
    protected $primaryKey = "kd_wilayah";
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['kd_wilayah'];
}
