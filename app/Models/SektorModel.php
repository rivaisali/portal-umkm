<?php

namespace App\Models;

use CodeIgniter\Model;

class SektorModel extends Model
{
    protected $table = "sektor";
    protected $primaryKey = "kd_sektor";
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['kd_sektor', 'nm_sektor'];
}
