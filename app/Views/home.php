<?= $this->extend('main'); ?>
<?= $this->section('content'); ?>
<section class="hero-6 bg-center position-relative overflow-hidden"
        style="background-image: url(<?=base_url()?>/assets_portal/images/hero-6-bg.png);" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <i class="mb-4 icon-lg sw-1_5 text-primary" data-feather="sunrise"></i>
                    <h1 class="font-weight-semibold mb-4 hero-6-title">Portal UMKM
                        <b>Kota Gorontalo</b>
                    </h1>
                    <p class="mb-5 text-muted">Portal UMKM Merupkan portal pendaftaran pelaku usaha UMKM yang berada di Kota Gorontalo</p>
                    <a href="<?php echo site_url('daftar');?>" class="btn btn-primary me-2"> Daftar Sekarang <i class="icon-sm ms-1"
                            data-feather="arrow-right"></i></a>
                </div>
                <div class="col-lg-6 col-sm-10 mx-auto ms-lg-auto me-lg-0">
                    <div class="mt-lg-0 mt-5">
                        <img src="<?=base_url()?>/assets_portal/images/portal.svg" alt="" class="img-xl-responsive" />
                    </div>
                </div>
            </div>
        </div>
    </section>
 <?= $this->endSection('content'); ?>