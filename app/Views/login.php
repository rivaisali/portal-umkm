<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Login - Portal UMKM Kota Gorontalo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta content="Themesbrand" name="author" />
    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- css -->
    <link href="<?php echo base_url('assets_portal/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets_portal/css/materialdesignicons.min.css'); ?>" rel="stylesheet"
        type="text/css" />
    <link href="<?php echo base_url('assets_portal/css/style.min.css'); ?>" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>

    <section class="bg-account-pages vh-100 d-flex align-items-center bg-center position-relative"
        style="background-image: url(<?php echo base_url('assets_portal/images/auth-bg.png'); ?>);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="bg-white shadow">
                        <div class="p-4">
                            <div class="text-center mt-3">
                                <a href="index-1.html">
                                    <img src="<?php echo base_url('assets_portal/images/logo-dark.png'); ?>" alt=""
                                        class="logo-dark" height="80" />
                                </a>
                                <div class="p-3">
                                    <?php if (session()->getFlashdata('msg')): ?>
                                    <div class="alert alert-danger"><?=session()->getFlashdata('msg')?></div>
                                    <?php endif;?>
                                    <p class="text-muted mt-2">Sign in to continue to Portal UMKM Kota Gorontalo.</p>
                                </div>
                                <form action="/auth/login" method="post">
                                    <div class="mb-3">
                                        <!-- <label for="username" class="form-label">Username</label> -->
                                        <input name="email" required="" placeholder="Username" id="username" type="text"
                                            class="form-control" value="" />
                                    </div>
                                    <div class="mb-3">
                                        <!-- <label for="userpassword" class="form-label">Password</label> -->
                                        <input name="password" required="" placeholder="Password" id="userpassword"
                                            type="password" class="form-control" value="" />
                                    </div>

                                    <div class="d-grid mt-3"><button type="submit"
                                            class="btn btn-primary btn-none">Masuk</button></div>
                                    <div class="mt-4 mb-0 text-center">

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-4">

                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </section>
    <!-- javascript -->
    <script src="<?php echo base_url('assets_portal/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets_portal/js/smooth-scroll.polyfills.min.js'); ?>"></script>

    <script src="https://unpkg.com/feather-icons"></script>

    <!-- App Js -->
    <script src="<?php echo base_url('assets_portal/js/app.js'); ?>"></script>
</body>'

</html>