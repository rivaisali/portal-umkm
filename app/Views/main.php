<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Portal UMKM Kota Gorontalo">
  <meta name="author" content="DINAS TENAGA KERJA, KOPERASI DAN UKM">
  <meta property="og:image" content="https://gorontalokota.go.id/img/logo.png" />
  <meta property="og:description"
    content="Portal Pendaftaran Usaha Mikro Kecil Menengah Kota Gorontalo" />
  <meta property="og:url" content="https://gorontalokota.go.id/" />
  <meta property="og:title" content="Kota Gorontalo Smart City" />
  <meta property="og:site_name" content="Kota Gorontalo Smart City" />
  <title>Home - Portal UMKM Kota Gorontalo</title>
    <!-- favicon -->
    <link rel="shortcut icon" href="https://gorontalokota.go.id/img/logo.png"/>

    <!-- css -->
    <link href="<?php echo base_url('assets_portal/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets_portal/css/materialdesignicons.min.css'); ?>" rel="stylesheet"
        type="text/css" />
    <link href="<?php echo base_url('assets_portal/css/style.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets_portal/css/multi-form.css?v2'); ?>" rel="stylesheet" type="text/css" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="20">
    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>

    <!--Navbar Start-->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="navbar">
        <div class="container">
            <!-- LOGO -->
            <a class="navbar-brand logo" href="index-1.html">
                <img src="<?=base_url()?>/assets_portal/images/logo-dark.png" alt="" class="logo-dark" height="68" />
                <img src="<?=base_url()?>/assets_portal/images/logo-light.png" alt="" class="logo-light" height="68" />
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="" data-feather="menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto navbar-center" id="navbar-navlist">
                    <li class="nav-item">
                        <a href="<?php echo site_url(''); ?>" class="nav-link active">Home</a>
                    </li>


                </ul>
                <a href="<?php echo site_url('daftar'); ?>" class="btn btn-sm rounded-pill nav-btn ms-lg-3">Daftar
                    Sekarang</a>
            </div>
        </div>
        <!-- end container -->
    </nav>
    <!-- Navbar End -->

    <?=$this->renderSection("content")?>

    <!-- javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets_portal/js/bootstrap.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets_portal/js/smooth-scroll.polyfills.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url('assets_portal/js/multi-form.js?v2'); ?>"></script>
    <script src="https://unpkg.com/feather-icons"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!-- App Js -->
    <script src="<?php echo base_url('assets_portal/js/app.js'); ?>"></script>
    <script>
        $(document).ready(function () {
                $('#kdkecamatan').change(function () {
                    var id = $(this).val();
                    $.ajax({
                        url: "<?php echo site_url('datakelurahan'); ?>",
                        method: "POST",
                        data: {
                            id: id
                        },
                        async: true,
                        dataType: 'json',
                        success: function (data) {
                            var html = '';
                            var i;
                            html += '<option value="0">Pilih Kelurahan</option>';
                            for (i = 0; i < data.length; i++) {
                                html += '<option value=' + data[i].kd_wilayah + '>' + data[i]
                                    .kelurahan + '</option>';
                            }
                            $('#kelurahan').html(html);

                        }
                    });
                    return false;
                });


                $('#kelurahan').change(function () {
                var id = $(this).val();
                $.ajax({
                    url: "<?php echo site_url('datakodepos'); ?>",
                    method: "POST",
                    data: {
                        id: id
                    },
                    async: true,
                    dataType: 'json',
                    success: function (data) {

                        $('#kodepos').val(data[0].kodepos);

                    }
                });
                return false;
                });
            });
                </script>
</body>

</html>