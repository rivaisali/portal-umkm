<!DOCTYPE html>
<html>

<head>
  <?=$this->include("admin/_partials/head.php")?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <?=$this->include("admin/_partials/navbar.php")?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url("assets/img/AdminLTELogo.png"); ?>" alt="AdminLTE Logo"
          class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">e-UMKM</span>
      </a>

      <!-- Sidebar -->
      <?=$this->include("admin/_partials/sidebar.php")?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Content Header (Page header) -->
      <?=$this->include("admin/_partials/breadcrumb.php")?>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
        <?=$this->renderSection('content')?>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?=$this->include("admin/_partials/footer.php")?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->
  <?=$this->include("admin/_partials/js.php")?>
</body>

</html>