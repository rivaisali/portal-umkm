<?=$this->extend('admin/overview');?>
<?=$this->section('content');?>
<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3><?=$pelaku_usaha?></h3>

          <p>Pelaku Usaha</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3><?=$usaha?></h3>

          <p>Usaha</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner">
          <h3><?=$jenis_usaha?></h3>

          <p>Jenis Usaha</p>
        </div>
        <div class="icon">
          <i class="ion ion-briefcase"></i>
        </div>
        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3><?=$sektor?></h3>

          <p>Sektor Usaha</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
      </div>
    </div>

    <div class="col-lg-6 col-6">
       <div class="card card-danger">
      <div class="card-header">
        <h3 class="card-title">Data Usaha Per-Klasifikasi </h3>
      </div>
      <div class="card-body">
        <canvas id="klasifikasiChart" style="height:330px; min-height:330px"></canvas>
      </div>
      <!-- /.card-body -->
    </div>
    </div>

   <div class="col-lg-6 col-6">
       <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Data Usaha Per-Kecamatan </h3>
      </div>
      <div class="card-body">
        <canvas id="kecamatanChart" style="height:330px; min-height:330px"></canvas>
      </div>
      <!-- /.card-body -->
    </div>
    </div>


    <!-- ./col -->
  </div>
  <!-- /.row -->

</div><!-- /.container-fluid -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
$(document).ready(function() {
    var klasifikasiChartCanvas = $('#klasifikasiChart').get(0).getContext('2d')
    var klasifikasiData        = {
      labels: [
          'Mikro',
          'Kecil',
          'Menengah'
      ],
      datasets: [
        {
          data: [<?=$kl_mikro?>,<?=$kl_kecil?>,<?=$kl_menengah?>],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12'],
        }
      ]
    }
    var klasifikasiOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    var klasifikasiChart = new Chart(klasifikasiChartCanvas, {
      type: 'doughnut',
      data: klasifikasiData,
      options: klasifikasiOptions
    });


     var kecamatanChartCanvas = $('#kecamatanChart').get(0).getContext('2d')
    var kecamatanChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    kecamatanChartData.datasets[0] = temp1
    kecamatanChartData.datasets[1] = temp0

    var kecamatanChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var kecamatanChart = new Chart(kecamatanChartCanvas, {
      type: 'bar',
      data: kecamatanChartData,
      options: kecamatanChartOptions
    })


});


</script>
<?=$this->endSection('content');?>