<?=$this->extend('admin/overview');?>
<?=$this->section('content');?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-8">

            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title">Sektor</h3> -->
                <div align="right"><a href="<?=base_url('/admin/sektor/create');?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambah</a></div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <?php if (!empty(session()->getFlashdata('message'))): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('message'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif;?>
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Sektor</th>
                    <th>#</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
$no = 1;
foreach ($sektor as $row) {
    ?>

                  <tr>
                  <td><?=$no++;?></td>
                        <td><?=$row->nm_sektor;?></td>
                        <td>
                            <a title="Edit" href="<?=base_url("admin/sektor/edit/$row->kd_sektor");?>" class="btn btn-sm btn-info">Edit</a>
                            <a title="Delete" href="<?=base_url("admin/sektor/delete/$row->kd_sektor")?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ?')">Delete</a>
                        </td>
                  </tr>
                  <?php
}
?>
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>No</th>
                    <th>Nama Sektor</th>
                    <th>#</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <?=$this->endSection('content');?>