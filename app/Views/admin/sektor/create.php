<?=$this->extend('admin/overview');?>
<?=$this->section('content');?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <?php if (!empty(session()->getFlashdata('error'))): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <h5><i class="icon fas fa-ban"></i> Alert!</h5>
          </hr />
          <?php echo session()->getFlashdata('error'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php endif;?>



        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Entri Data Sektor</h3>
          </div>

          <!-- form start -->
          <form method="post" action="<?=base_url('admin/sektor/store')?>">
            <?=csrf_field();?>
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInput">Nama Sektor</label>
                <input type="text" class="form-control" id="nm_sektor" name="nm_sektor" placeholder="Nama Sektor"
                  value="<?=old('nm_sektor');?>">
              </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer" align="right">
              <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Simpan</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
      <!--/.col (left) -->

    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<?=$this->endSection('content');?>