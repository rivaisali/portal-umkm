<?=$this->extend('admin/overview');?>
<?=$this->section('content');?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <div class="card">
              <div class="card-header">
                <!-- <h3 class="card-title">Sektor</h3> -->

              </div>
              <!-- /.card-header -->
              <div class="card-body">

                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Wilayah</th>
                    <th>Provinsi</th>
                    <th>Kabupaten/Kota</th>
                    <th>Kecamatan</th>
                    <th>Kelurahan</th>
                    <th>Kode Pos</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
$no = 1;
foreach ($wilayah as $row) {
    ?>

                  <tr>
                  <td><?=$no++;?></td>
                        <td><?=$row->kd_wilayah;?></td>
                        <td><?=$row->namapropinsi;?></td>
                        <td><?=$row->namakabkota;?></td>
                        <td><?=$row->namakecamatan;?></td>
                        <td><?=$row->kelurahan;?></td>
                        <td><?=$row->kodepos;?></td>
                  </tr>
                  <?php
}
?>
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>No</th>
                    <th>Kode Wilayah</th>
                    <th>Provinsi</th>
                    <th>Kabupaten/Kota</th>
                    <th>Kecamatan</th>
                    <th>Kelurahan</th>
                    <th>Kode Pos</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <?=$this->endSection('content');?>