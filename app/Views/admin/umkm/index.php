<?=$this->extend('admin/overview');?>
<?=$this->section('content');?>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">

        <div class="card">
          <div class="card-header">
            <!-- <h3 class="card-title">Sektor</h3> -->

          </div>
          <!-- /.card-header -->
          <div class="card-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama Pemilik</th>
                  <th>Nama Usaha</th>
                  <th>Jenis Usaha</th>
                  <th>Klasifikasi Usaha</th>
                  <th>Sektor Usaha</th>
                  <th>Alamat Usaha</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
                <?php
$no = 1;
foreach ($umkm as $row) {
    ?>

                <tr>
                  <td><?=$no++;?></td>
                  <td><?=$row->nik;?></td>
                  <td><?=$row->nama;?></td>
                  <td><?=$row->nama_usaha;?></td>
                  <td><?=$row->jenis_usaha;?></td>
                  <td><?=$row->klasifikasi_usaha;?></td>
                  <td><?=$row->nm_sektor;?></td>
                  <td><?=$row->alamat;?></td>
                  <td>
                   <a title="Detail Usaha" href="<?=base_url("admin/sektor/edit/$row->kd_usaha");?>"
                      class="btn btn-xs btn-info"><i class="fa fa-list"></i></a> |
                    <a title="Edit" href="<?=base_url("admin/sektor/edit/$row->kd_usaha");?>"
                      class="btn btn-xs btn-warning"><i class="fa fa-pencil-alt"></i></a> |
                    <a title="Delete" href="<?=base_url("admin/sektor/delete/$row->kd_usaha")?>"
                      class="btn btn-xs btn-danger"
                      onclick="return confirm('Apakah Anda yakin ingin menghapus data ?')"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php
}
?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama Pemilik</th>
                  <th>Nama Usaha</th>
                  <th>Jenis Usaha</th>
                  <th>Klasifikasi Usaha</th>
                  <th>Sektor Usaha</th>
                  <th>Alamat Usaha</th>
                  <th>#</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<?=$this->endSection('content');?>