<!-- Contact us start -->
<?=$this->extend('main');?>
<?=$this->section('content');?>
<section class="section" id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h2 class="fw-bold mb-4">Daftarkan Usaha Anda</h2>
				<p class="text-muted mb-5">Masukan data pelaku usaha dan data usaha anda dengan lengkap dan benar.</p>
				<div>
					<form id="myForm" method="POST" action="<?=base_url('daftar/umkm')?>"
						onkeydown="return event.key != 'Enter';" enctype="multipart/form-data">
						<?=csrf_field();?>
						<p id="error-msg" class="badge badge-danger"><?php echo session()->getFlashdata('error'); ?></p>
						<div id="simple-msg" class="badge badge-success"><?php echo session()->getFlashdata('message'); ?></div>
						<div class="tab">
							<div class="row">
								<div class="col-md-12">
									<div class="mb-4">
										<input type="hidden" name="token" value="<?=$_GET['t']?>">
										<label class="text-muted form-label">No Induk Kependudukan</label>
										<input type="number" required class="form-control" name="nik" placeholder="NIK" />
									</div>
									<div class="mb-4 pb-2">
										<label class="text-muted form-label">Nama Lengkap</label>
										<input type="text" required name="nama" placeholder="Nama Lengkap" id="nama"
											class="form-control">
									</div>
									<div class="mb-4">
										<label for="npwp" class="text-muted form-label">No. NPWP (* Jika ada</label>
										<input name="npwp" id="npwp" type="text" class="form-control"
											placeholder="NPWP">
									</div>
								</div>

								<div class="col-lg-6">
									<div class="mb-4">
										<label class="text-muted form-label">No. Telp</label>
										<input name="notelp" value="<?=$_GET['u'] != null ? $_GET['u'] : "";?>" required type="text" class="form-control"
											placeholder="No. Telp">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="mb-4">
										<label for="email" class="text-muted form-label">Email</label>
										<input name="mail" required type="email" class="form-control"
											placeholder="E-mail">
									</div>
								</div>

							</div>
						</div>


						<div class="tab">
							<div class="row">
								<div class="col-md-12">
									<div class="mb-4">
										<label for="subject" class="text-muted form-label">Nama Usaha</label>
										<input type="text" required class="form-control"" name=" nama_usaha"
											placeholder="Nama Usaha" />
									</div>
									<div class="mb-4 pb-2">
										<label for="comments" class="text-muted form-label">Alamat Usaha</label>
										<input type="text" required name="alamat" placeholder="Alamat (* Nama Jalan"
											class="form-control">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="mb-4">
										<label for="name" class="text-muted form-label">Kecamatan</label>
										<select name="kdkecamatan" required id="kdkecamatan" class="form-control">
											<option value=""> Pilih Kecamatan </option>
											<?php
foreach ($wilayah as $d) {
    $kodekec = substr($d->kd_wilayah, 0, 8);
    echo '<option value="' . $d->namakecamatan . '">' . $d->namakecamatan . '</option>';
}
?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="mb-4">
										<label for="kelurahan" class="text-muted form-label">Kelurahan</label>
										<select name="kdkelurahan" required id="kelurahan" class="form-control">
											<option value=""> Pilih Kelurahan </option>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="mb-4">
										<label for="kodepos" class="text-muted form-label">Kode Pos</label>
										<input name="kodepos" required id="kodepos" type="text" readonly
											class="form-control" placeholder="Kode Pos">
									</div>
								</div>

								<div class="col-lg-6">
									<div class="mb-4">
										<label for="jenis_usaha" class="text-muted form-label">Jenis Usaha</label>
										<input name="jenis_usaha" required id="jenis_usaha" type="text" class="form-control" placeholder="Jenis Usaha (* Makanan, Pembuatan Kue, Kerajinan dll">
									</div>
								</div>

								<div class="col-lg-6">
									<div class="mb-4">

										<label for="sektor" class="text-muted form-label">Sektor Usaha</label>
										<select name="sektor" required class="form-control">
											<option value=""> Pilih Sektor </option>
											<?php
foreach ($sektor as $d) {
    echo '<option value="' . $d->kd_sektor . '">' . $d->nm_sektor . '</option>';
}
?>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="mb-4 pb-2">
										<label for="modalusaha" class="text-muted form-label">Modal Usaha</label>
										<input name="modalusaha" required id="modalusaha" type="text"
											class="form-control" placeholder="Modal Usaha">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="mb-4">
										<label for="klasifikasiusaha" class="text-muted form-label">Klasifikasi
											Usaha</label>

										<input name="klasifikasiusaha" required readonly id="klasifikasiusaha" type="text"
											class="form-control" placeholder="Klasifikasi Usaha">
									</div>
								</div>
								<div class="col-md-12">
									<div class="mb-4 pb-2">
										<label for="tahun_awal" class="text-muted form-label">Tahun Awal Usaha</label>
										<input type="number" name="tahun_awal" required id="tahun_awal" class="form-control"
											placeholder="Tahun Awal Usaha" maxlength="4" minlength="4">
									</div>
								</div>
							</div>
						</div>


						<div class="tab">
							<div class="row">
								<div class="col-lg-6">
									<div class="mb-4">
										<label for="tk_pria" class="text-muted form-label">Tenaga Kerja
											Laki-Laki</label>
										<input name="tk_pria" maxlength="4" required id="tk_pria" type="number" class="form-control"
											placeholder="Tenaga Kerja Laki-laki">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="mb-4">
										<label for="tk_wanita" class="text-muted form-label">Tenaga Kerja
											Perempuan</label>
										<input name="tk_wanita" maxlength="4" required id="tk_wanita" type="number"
											class="form-control" placeholder="Tenaga Kerja Perempuan">
									</div>
								</div>
								<div class="col-md-12">
									<div class="mb-4">
										<label for="jenis_izin" class="text-muted form-label">Jenis Izin</label>
										<input type="text" required class="form-control" name="jenis_izin"
											placeholder="Jenis Izin yang sudah ada (* IUMK, SIUP dll" />
									</div>
									<div class="mb-4">
										<label for="no_izin" class="text-muted form-label">No Izin</label>
										<input type="text" required class="form-control" name="no_izin"
											placeholder="No Izin" />
									</div>
									<div class="mb-4">
										<label for="nib" class="text-muted form-label">No. Induk Berusaha (NIB)</label>
										<input type="text"  class="form-control" name="nib"
											placeholder="No. Induk Berusaha (NIB)" />
									</div>
									<div class="mb-4">
										<label for="tgl_terbit_nib" class="text-muted form-label">Tgl. Terbit
											NIB</label>
										<input type="date" class="form-control"" name=" tgl_terbit_nib"
											placeholder="Tgl. Terbit NIB" />
									</div>
								</div>
							</div>
						</div>

						<div class="tab">
							<div class="row">
								<div class="col-lg-12">
									<div class="mb-4">
										<label for="filektp" class="text-muted form-label">Foto KTP</label><br>
										<span class="badge badge-warning">Ukuran Foto KTP Max 1 Mbps</span>
										<input name="filektp" required id="filektp" type="file" class="form-control"
											placeholder="Foto KTP">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="mb-4">
										<label for="filetempatusaha" class="text-muted form-label">Foto Tempat
											Usaha</label><br>
											<span class="badge badge-warning">Ukuran Foto Max 1 Mbps</span>
										<input name="filetempatusaha" required id="filetempatusaha" type="file"
											class="form-control" placeholder="Foto Tempat Usaha">
									</div>
								</div>
								<div class="col-md-12">
									<div class="mb-4">
										<label for="filesuket" class="text-muted form-label">Surat Keterangan Usaha (*
											Tidak Wajib</label><br>
											<span class="badge badge-warning">Ukuran Foto Max 1 Mbps</span>
										<input type="file" class="form-control" name="filesuket"
											placeholder="Surat Keterangan Usaha (* Tidak Wajib" />
									</div>

								</div>
							</div>
						</div>
						<div style="overflow:auto;">
							<div style="float:right; margin-top: 5px;">
								<button type="button" class="previous btn btn-warning">Kembali</button>
								<button type="button" class="next btn btn-primary">Lanjut</button>
								<button type="submit" class="submit btn btn-success">Daftar</button>
							</div>
						</div>
						<div style="text-align:center;margin-top:40px;">
							<span class="step">1</span>
							<span class="step">2</span>
							<span class="step">3</span>
							<span class="step">4</span>
						</div>
					</form>
				</div>
			</div>
			<!-- end col -->
			<div class="col-lg-5 ms-lg-auto">
				<div class="mt-5 mt-lg-0">
					<img src="
									<?=base_url()?>/assets_portal/images/contact.png" alt="" class="img-fluid d-block" />
					<p class="text-muted mt-5 mb-3">
						<i class="me-2 text-muted icon icon-xs" data-feather="mail"></i>
						umkm@gorontalokota.go.id
					</p>
					<p class="text-muted mb-3">
						<i class="me-2 text-muted icon icon-xs" data-feather="phone"></i> (0435) 821861
					</p>
					<p class="text-muted mb-3">
						<i class="me-2 text-muted icon icon-xs" data-feather="map-pin"></i>
						Jl. Drs. Achmad Nadjamuddin No.35, Wumialo, Kota Tengah, Kota Gorontalo, Gorontalo 96138
					</p>
					<ul class="list-inline pt-4">
						<li class="list-inline-item me-3">
							<a href="javascript: void(0);" class="social-icon icon-mono avatar-xs rounded-circle">
								<i class="icon-xs" data-feather="facebook"></i>
							</a>
						</li>
						<li class="list-inline-item me-3">
							<a href="javascript: void(0);" class="social-icon icon-mono avatar-xs rounded-circle">
								<i class="icon-xs" data-feather="twitter"></i>
							</a>
						</li>
						<li class="list-inline-item">
							<a href="javascript: void(0);" class="social-icon icon-mono avatar-xs rounded-circle">
								<i class="icon-xs" data-feather="instagram"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end container -->
</section>
<!-- Contact us end -->
<?=$this->endSection('content');?>